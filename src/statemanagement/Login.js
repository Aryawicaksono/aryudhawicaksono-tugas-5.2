import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

const Login = ({navigation, route}) => {
  const url = 'https://staging.api.autotrust.id/api/v1/';
  const dispatch = useDispatch();
  const {isLoggedIn, userData} = useSelector(state => state.auth);

  React.useEffect(() => {
    if (isLoggedIn) {
      // console.log('cek userData', userData);
      navigation.replace('Home');
    }
  }, []);

  const onLogin = async () => {
    const data = {
      email,
      password,
    };
    try {
      const response = await fetch(`${url}user/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      const result = await response.json();
      // console.log('Success:', result);

      if (result.code === 200) {
        const data = result;
        dispatch({type: 'LOGIN-SUCCESS', data});
        ToastAndroid.showWithGravity(
          'Login Sukses',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
        navigation.replace('Home');
      }
    } catch (error) {
      console.error('Error:', error);
      ToastAndroid.showWithGravity(
        'Login gagal, email atau password salah',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    }
  };
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 20, color: '#000', fontWeight: '700'}}>
        {' '}
        Login{' '}
      </Text>
      <View style={{width: '80%', marginTop: 15}}>
        <Text>Masukkan Email</Text>
        <TextInput
          value={email}
          placeholder="Masukkan Email"
          keyboardType="email-address"
          style={{
            width: '100%',
            borderRadius: 6,
            borderColor: '#dedede',
            borderWidth: 1,
            paddingHorizontal: 10,
            marginTop: 15,
          }}
          onChangeText={text => setEmail(text)}
        />
      </View>
      <View style={{width: '80%', marginTop: 15}}>
        <Text>Masukkan Password</Text>
        <TextInput
          placeholder="Masukkan Password"
          secureTextEntry={true}
          value={password}
          style={{
            width: '100%',
            borderRadius: 6,
            borderColor: '#dedede',
            borderWidth: 1,
            paddingHorizontal: 10,
            marginTop: 15,
          }}
          onChangeText={text => setPassword(text)}
        />
      </View>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 10,
          paddingHorizontal: 12,
          backgroundColor: 'green',
          borderRadius: 6,
          marginTop: 20,
        }}
        onPress={() => onLogin()}>
        <Text style={{color: '#fff', fontSize: 14, fontWeight: '500'}}>
          Log in
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;
